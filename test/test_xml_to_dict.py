import unittest
from xml.etree import cElementTree as ElementTree
from xml_to_dict import XMLToDict


class TestXMLToDict(unittest.TestCase):

    def setUp(self):
        input_xml = f'''
            <root>
        	<to>user id</to>
        	<time>1626941727</time>
        	<params>
        		<click>click1</click>
        		<click>click2</click>
        		<click>click3</click>
        		<additional>
        			<subgroup>group1&amp;val2</subgroup>
        			<subgroup>group1&amp;val3</subgroup>
        			<subgroup>group1&amp;val4</subgroup>
        		</additional>
        	</params>
        	<x>
        		<a>1</a>
        		<b>2</b>
        	</x>
        </root>
            '''
        self.xml_to_dict = XMLToDict(input_xml)

    def test_main(self):
        d = {'to': 'user id',
             'time': 1626941727,
             'params': {'click': ['click1', 'click2', 'click3'],
                        'additional': {'subgroup': ['group1&val2', 'group1&val3', 'group1&val4']}},
             'x': {'a': 1,
                   'b': 2}}
        self.assertEqual(self.xml_to_dict.main(), d)

    def test_collect_xml_to_dict(self):
        input_xml = f'''
                    <root>
                	<to>user id</to>
                	<time>1626941727</time>
                	<params>
                		<click>click1</click>
                		<click>click2</click>
                		<click>click3</click>
                	</params>
                	<x>
                		<a>1</a>
                		<b>2</b>
                	</x>
                </root>
                    '''
        test_parse = ElementTree.XML(text=input_xml)
        d = {'root': {'to': 'user id',
                      'time': 1626941727,
                      'params': {'click': ['click1', 'click2', 'click3']},
                      'x': {'a': 1,
                            'b': 2}}}
        self.assertEqual(self.xml_to_dict.collect_xml_to_dict(test_parse), d)
