## XML string or XML file to dictionary (Python native)

### Start script:
1) Paste your code to *data.xml* or in *test_xml_to_dict.py* (variable *input_xml*);
2) Start script *xml_to_dict.py* (if you paste to *data.xml* file) or *test_xml_to_dict.py* (if you change variable *input_xml*);
3) Open file *xml_to_dict.log* (upon successful execution of the code the result will be shown in string "INFO").