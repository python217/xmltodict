import logging
from xml.etree import cElementTree as ElementTree
from collections import defaultdict

logging.basicConfig(level=logging.INFO, filename="xml_to_dict.log", filemode="a")


class XMLToDict:

    def __init__(self, input_data):
        self.input_data = input_data

    @staticmethod
    def reform_to_dict_structure(xml_data, temp_dict):
        """
        Function takes over xml data and data from temporary dictionary (default dict) in order to build final
        structure of base dictionary

        :param xml_data:
        :param temp_dict:
        :return:
        """
        return {xml_data.tag: {key: value[0] if len(value) == 1 else value for key, value in temp_dict.items()}}

    @staticmethod
    def fill_values(xml_data, xml_dict):
        """
        Function takes over xml data and base dictionary for fill data (values) by tags.

        :param xml_data:
        :param xml_dict:
        :return:
        """
        try:
            if values := int(xml_data.text.strip()):
                xml_dict[xml_data.tag] = values
        except ValueError:
            if values := xml_data.text.strip():
                xml_dict[xml_data.tag] = values
        except Exception as e:
            logging.error(f"fill_values: {e}")
            pass
        return xml_dict

    def collect_xml_to_dict(self, xml_data):
        """
        Function takes over xml data and collect them to dictionary.
        variables:
            xml_data - base dictionary for build correct structure.
            temp_dict - default dict need to simplify the work with dictionaries. In this case,
            the scope is the function.

        :param xml_data:
        :return:
        """
        temp_dict = defaultdict(list)
        xml_dict: dict = {xml_data.tag: xml_data.attrib if xml_data.attrib else ''}
        try:
            if child_elements_list := list(xml_data):
                for child in map(self.collect_xml_to_dict, child_elements_list):
                    for key, value in child.items():
                        temp_dict[key].append(value)
                xml_dict: dict = self.reform_to_dict_structure(xml_data=xml_data, temp_dict=temp_dict)
        except Exception as e:
            logging.error(f"collect_xml_to_dict: {e}")

        return self.fill_values(xml_data=xml_data, xml_dict=xml_dict)

    def main(self):
        """
        Function checks for the presence of data in the line, if there is no data, then by default
        get data from the file 'data.xml' and call function for parse.
        """
        try:
            if self.input_data:
                output = self.collect_xml_to_dict(ElementTree.XML(text=self.input_data)).get('root')
            else:
                output = self.collect_xml_to_dict(ElementTree.parse("data.xml").getroot()).get('root')
        except Exception as e:
            logging.error(f"Unknown exception: {e}")

        logging.info(f" Script have finished successful. OUTPUT: {output}")
        return output


if __name__ == '__main__':
    input_xml = f''''''
    xmltodict = XMLToDict(input_xml)
    xmltodict.main()
